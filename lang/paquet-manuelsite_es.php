<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-manuelsite?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'manuelsite_description' => 'Este plugin instala un icono de ayuda permitiendo mostrar desde no importa qué página del espacio privado el manual de redacción del sitio. Este manual es un artículo del sitio. El plugin proporciona asimismo un ítem de Preguntas Frecuentes (FAQ) comunes, pudiendo insertarse fácilmente en su manual. ',
	'manuelsite_nom' => 'Manual de redacción del sitio',
	'manuelsite_slogan' => 'Un manual característico de su sitio para sus redactores'
);
