<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-manuelsite?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'manuelsite_description' => 'This plugin displays an help icon on every page of the private space showing the Website Editor Handbook. This handbook is an article of the website. The plugin also provides a set of Frequently Asked Questions that can be easily inserted in your handbook.',
	'manuelsite_nom' => 'Website Editor Handbook',
	'manuelsite_slogan' => 'A handbook specific to your site for your editors'
);
