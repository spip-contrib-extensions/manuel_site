<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/manuelsite?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'configurer_explication' => 'Questo plugin installa un’icona di aiuto che permette di visualizzare da qualsiasi pagina dell’area riservata il manuale di scrittura del sito. Questo manuale è @texte@. Il suo scopo è spiegare ai redattori l’architettura del sito, in quale sezione memorizzare cosa, come codificare e installare un video... In breve, tutto quello che vuoi e che è specifico del tuo sito.',
	'configurer_explication_l_article' => '<a href="@url@" title="Manuale di redazione">l’articolo #@idart@</a> del vostro sito.',
	'configurer_explication_un_article' => 'un articolo del sito.',
	'configurer_titre' => 'Impostare il manuale di redazione del sito',

	// E
	'erreur_article' => 'L’articolo del manuale definito nella configurazione del plugin non è stato trovato: #@idart@',
	'erreur_article_publie' => 'L’articolo del manuale definito nella configurazione del plugin non è pubblicato online: <a href="@url@">#@idart@</a>',
	'erreur_pas_darticle' => 'L’articolo del manuale non è definito nella configurazione del plugin',
	'explication_afficher_bord_gauche' => 'Visualizza l’icona del manuale in alto a sinistra (altrimenti il ​​manuale verrà visualizzato in colonna)',
	'explication_background_color' => 'Immettere il colore di sfondo dell’area di visualizzazione del manuale',
	'explication_cacher_public' => 'Nascondi questo articolo nello spazio pubblico, inclusi i feed RSS',
	'explication_email' => 'Email di contatto per i redattori',
	'explication_faq' => 'Di seguito troverai i codici dei blocchi generici che possono essere utilizzati per scrivere il tuo manuale. Il testo corrispondente a ciascun codice viene visualizzato (senza formattazione) passandoci sopra con il mouse. Copia/incolla semplicemente il codice desiderato nell’area di testo del tuo articolo. <br />Per non visualizzare la domanda, aggiungi <i>|q=non</i>. <br />Per aggiungere parametri, aggiungi <i>|params=p1:v1 ;p2:v2</i>.',
	'explication_formu' => 'Indicare il nome dell’oggetto editoriale che raccoglie le richieste di aiuto (per tenerne conto è necessario lasciare vuoto il campo e-mail in alto)',
	'explication_id_article' => 'Immettere il numero dell’articolo che contiene il manuale',
	'explication_intro' => 'Testo introduttivo al manuale (verrà posto prima del capitolo)',

	// F
	'fermer_le_manuel' => 'Chiudi manuale',

	// H
	'help' => 'Aiuto:',
	'help2' => ' invia la tua richiesta di assistenza',

	// I
	'intro' => 'Ce document a pour but d’aider les rédacteurs à l’utilisation du site. Il vient en complément du document intitulé « [Cours SPIP pour rédacteurs->@url@] » qui est une aide globale à l’utilisation de SPIP. Vous y trouverez une description de l’architecture du site, de l’aide technique sur des points particuliers...

Questo documento ha lo scopo di aiutare i redattori ad utilizzare il sito. Completa il documento intitolato "[Corso SPIP per redattori->@url@]", che è un aiuto generale per l’uso di SPIP. Troverai una descrizione dell’architettura del sito, assistenza tecnica su punti specifici...', # MODIF

	// L
	'label_afficher_bord_gauche' => 'Visualizzazione',
	'label_background_color' => 'Colore di sfondo',
	'label_cacher_public' => 'Nascondere',
	'label_email' => 'Email',
	'label_formu' => 'Modulo',
	'label_id_article' => 'Num. dell’articolo',
	'label_intro' => 'Introduzione',
	'legende_apparence' => 'Aspetto',
	'legende_contenu' => 'Contenuto',

	// T
	'titre_faq' => 'FAQ del Manuale di redazione',
	'titre_manuel' => 'Manuale di redazione del sto',
	'titre_menu' => 'Manuale di redazione del sto'
);
