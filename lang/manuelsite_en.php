<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/manuelsite?lang_cible=en
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'configurer_explication' => 'This plugin displays an help icon on every page of the private space showing the Website Editor Handbook. This handbook is @texte@. Its purpose is to explain the architecture of the site to the editors, in which section to store what, how to encode and install a video… So, all that you want and which is specific to your website.',
	'configurer_explication_l_article' => '<a href="@url@" title="Website Editor Handbook">the article #@idart@</a> of your website.',
	'configurer_explication_un_article' => 'an article of the website.',
	'configurer_titre' => 'Configure the Website Editor Handbook',

	// E
	'erreur_article' => 'The article of the handbook defined in the plugin’s configuration is not found: #@idart@',
	'erreur_article_publie' => 'The article defined in the configuration of the handbook plugin is not published online: <a href="@url@">#@idart@</a>',
	'erreur_pas_darticle' => 'The article of the handbook is not defined in the plugin’s configuration',
	'explication_afficher_bord_gauche' => 'Display the handbook’s icon in the top left corner (if not, the handbook will be displayed in a column)',
	'explication_background_color' => 'Type in the background color of the handbook display area',
	'explication_cacher_public' => 'Hide this article in the public space, even in rss flow',
	'explication_email' => 'Contact email for editors',
	'explication_faq' => 'Listed below are generic blocks codes which can be used to write your handbook. The text for each code is displayed (without formatting) when rolling over it. Just copy / paste the code in the textarea of your article.<br />To hide the question, add the parameter <i>|q=no</i>.<br />To add other parameters, add <i>|params=p1:v1;p2:v2</i>.',
	'explication_formu' => 'Enter the name of the editorial object that will be used to collect requests for assistance (to be taken into account, the e-mail field above must be left empty)',
	'explication_id_article' => 'Type in the number of the article which contains the handbook',
	'explication_intro' => 'Introduction text of the handbook (will be placed before the introduction of the article)',

	// F
	'fermer_le_manuel' => 'Close the handbook',

	// H
	'help' => 'Help : ',
	'help2' => 'submit your application for assistance',

	// I
	'intro' => 'The purpose of this document is to help the editors with the use of the site. It comes in complement from the document “[How to use SPIP as an author->@url@]” which is a global help with the usage of SPIP. You will find there a description of the architecture of the site, of the technical assistance on particular points…', # MODIF

	// L
	'label_afficher_bord_gauche' => 'Display',
	'label_background_color' => 'Background color',
	'label_cacher_public' => 'Hide',
	'label_email' => 'Email',
	'label_formu' => 'Form',
	'label_id_article' => 'Article number',
	'label_intro' => 'Introduction',
	'legende_apparence' => 'Appearance',
	'legende_contenu' => 'Contents',

	// T
	'titre_faq' => 'FAQ of the Website Editor Handbook',
	'titre_manuel' => 'Website Editor Handbook',
	'titre_menu' => 'Website Editor Handbook'
);
