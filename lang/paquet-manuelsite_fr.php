<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// Fichier source, a modifier dans https://git.spip.net/spip-contrib-extensions/manuel_site.git
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'manuelsite_description' => 'Ce plugin installe une icône d’aide permettant d’afficher depuis n’importe quelle page de l’espace privé le manuel de rédaction du site. Ce manuel est un article du site. Le plugin fournit également un jeu d’item de Foire Aux Questions courants pouvant être insérés facilement dans votre manuel.',
	'manuelsite_nom' => 'Manuel de rédaction du site',
	'manuelsite_slogan' => 'Un manuel spécifique à votre site pour vos rédacteurs'
);
