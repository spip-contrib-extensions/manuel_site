<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/manuelsite?lang_cible=es
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// C
	'configurer_explication' => 'Este plugin instala un icono de ayuda permitiendo mostrar desde no importa qué página del espacio privado el manual de redacción del sitio. Este manual es @texte@ Tiene por objetivo explicar a los redactores la arquitectura del sitio, en qué sección ubicar algo, cómo codificar e instalar un vídeo... En fin, todo lo que desee y que sea característico de su sitio.',
	'configurer_explication_l_article' => '<a href="@url@" title="Manual de redacción">el artículo #@idart@</a> de su sitio.',
	'configurer_explication_un_article' => 'un artículo del sitio. ',
	'configurer_titre' => 'Configurar el manual de redacción del sitio',

	// E
	'erreur_article' => 'El artículo definido en la configuración del plugin no se encuentra: #@idart@',
	'erreur_article_publie' => 'El artículo del manual definido en la configuración del plugin no está publicado en línea: <a href="@url@">#@idart@</a>',
	'erreur_pas_darticle' => 'El artículo del manual no está definido en la configuración del plugin',
	'explication_afficher_bord_gauche' => 'Mostrar el icono del manual arriba a la izquierda (si no el manual se mostrará en columna)',
	'explication_background_color' => 'Introduzca el color de fondo de la zona de visualización del manual',
	'explication_cacher_public' => 'Ocultar este artículo en el espacio público, canal rss incluido. ',
	'explication_email' => 'Correo electrónico de contacto para los redactores',
	'explication_faq' => 'Encontrará aquí los códigos de los bloques genéricos que se utilizan para redactar su manual. El texto correspondiente a cada código se muestra (sin formato) a la vista. Basta con copiar/pegar el código deseado en el área de texto de su artículo.<br />Para no mostrar la cuestión, añadir <i>|q=non</i>.<br />Para agregar la configuración, añadir <i>|params=p1:v1;p2:v2</i>.',
	'explication_id_article' => 'Introduzca el número del artículo que contiene el manual',
	'explication_intro' => 'Texto de introducción al manual (será emplazado antes de la entradilla)',

	// F
	'fermer_le_manuel' => 'Cerrar el manual',

	// H
	'help' => 'Ayuda:',

	// I
	'intro' => 'Este documento tiene por objetivo ayudar a los redactores con el uso del sitio. Complementa al documento titulado «[Curso SPIP para redactores->@url@]» que supone una ayuda global al uso de SPIP. Aquí encontrará una descripción de la arquitectura del sitio, de la ayuda técnica en aspectos concretos...', # MODIF

	// L
	'label_afficher_bord_gauche' => 'Visualización',
	'label_background_color' => 'Color de fondo',
	'label_cacher_public' => 'Ocultar',
	'label_email' => 'Correo electrónico',
	'label_id_article' => 'Nº del artículo',
	'label_intro' => 'Introducción',
	'legende_apparence' => 'Apariencia',
	'legende_contenu' => 'Contenido',

	// T
	'titre_faq' => 'FAQ del Manual de redacción',
	'titre_manuel' => 'Manual de redacción del sitio',
	'titre_menu' => 'Manual de redacción del sitio'
);
