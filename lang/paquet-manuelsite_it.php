<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-manuelsite?lang_cible=it
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'manuelsite_description' => 'Questo plugin installa un’icona di aiuto che permette di visualizzare da qualsiasi pagina dell’area riservata il manuale di redazione del sito. Questo manuale è un articolo del sito. Il plug-in fornisce anche una serie di elementi comuni delle domande frequenti che possono essere facilmente inseriti nel manuale.',
	'manuelsite_nom' => 'Manuale di redazione del sito',
	'manuelsite_slogan' => 'Un manuale specifico per il tuo sito per i tuoi redattori'
);
