<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-manuelsite?lang_cible=ru
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'manuelsite_description' => 'Этот плагин отображает значок справки на каждой странице административной части, показывая Руководство редактора сайта. Это руководство является статьей на сайте. Плагин также предоставляет набор часто задаваемых вопросов, которые могут быть легко добавлены в ваше Руководство.',
	'manuelsite_nom' => 'Руководство редактора сайта',
	'manuelsite_slogan' => 'Справочник особенностей вашего сайта для ваших редакторов'
);
