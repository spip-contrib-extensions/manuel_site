<?php
// This is a SPIP language file  --  Ceci est un fichier langue de SPIP
// extrait automatiquement de https://trad.spip.net/tradlang_module/paquet-manuelsite?lang_cible=sk
// ** ne pas modifier le fichier **

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

$GLOBALS[$GLOBALS['idx_lang']] = array(

	// M
	'manuelsite_description' => 'Tento zásuvný modul nainštaluje ikonu pomocníka, ktorá umožňuje zobraziť hociktorú stránku tohto manuálu na publikovanie zo súkromnej zóny. Tento manuál je článok na stránke. Zásuvný modul ponúka aj časť Časté otázky, ktorú môžete do svojho manuálu ľahko vložiť.',
	'manuelsite_nom' => 'Manuál na publikovanie stránky',
	'manuelsite_slogan' => 'Špeciálny manuál na vašu stránku pre vašich redaktorov'
);
