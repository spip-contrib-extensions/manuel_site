<?php
/**
 * Plugin Manuel du site
 * 
 * Utilisation des pipelines dans l'espace privé
 * 
 * @package SPIP\Manuelsite\Pipelines
 */
 
if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Insertion dans le pipeline body_prive (SPIP)
 * 
 * On ajoute dans le body l'icone et l'article du manuel du site
 * si la configuration "afficher_bord_gauche" n'est pas activée
 * 
 * @param string $flux
 * 		Le contenu de la page
 * @return string $flux
 * 		Le contenu de la page modifiée
 */
function manuelsite_body_prive($flux) {
	$flux .= recuperer_fond('prive/manuelsite');
	
	return $flux;
}

/**
 * Insertion dans le pipeline affiche_gauche (SPIP)
 * 
 * On affiche le bloc des contenus possibles de faq dans la colonne de l'article de manuel
 * 
 * @param array $flux
 * 		Le contexte du pipeline
 * @return array $flux
 * 		Le contexte du pipeline modifié
 */
function manuelsite_affiche_gauche($flux){
	// Si c'est un article en edition ou un article dans le prive,
	// on propose le formulaire, si l'article n'existe pas encore, on ne fait rien
	if (
		in_array($flux["args"]["exec"], array('article', 'article_edit')) 
	 	and !empty($flux["args"]["id_article"])
	 	and $conf_manuelsite = lire_config('manuelsite')
	 	and isset($conf_manuelsite['id_article'])
	 	and $conf_manuelsite['id_article']
	 	and $conf_manuelsite['id_article'] == $flux['args']['id_article']
	){
		$flux["data"] .= recuperer_fond('prive/squelettes/navigation/bloc_faq');
	}
	
	return $flux;
}

/**
 * Fonction retournant les blocs de FAQ utilisables
 * 
 * Parcourt tous les plugins à la recherche de fichiers de langue sous la forme 
 * /lang/faq-nom_plugin_fr.php afin de les afficher dans le bloc de choix d'éléments de faq
 * 
 * @return string $texte
 * 		Le contenu du bloc
 */
function manuelsite_lister_blocs_faq() {
	include_spip('inc/plugin');
	
	$texte = "\n<div>";

	$modules = array();

	foreach (liste_plugin_actifs() as $plugin) {
		$fichiers = preg_files(_DIR_PLUGINS.$plugin['dir'].'/lang/faq-[a-z_]+\.php$');
		foreach ($fichiers as $fichier) {
			if (preg_match(',/(faq-[a-z]+)_([a-z_]+)\.php$,', $fichier, $r))
				$modules[$plugin['nom']] = $r[1] ;
		}
	}
	ksort($modules);
	if (count($modules) > 0) {
		$texte .= "\n<ul class=\"faq\">" ;
		foreach ($modules as $nom_module => $dir_module) {
			$texte .= "\n<li><b>".typo($nom_module)."</b>";
			$texte .= manuelsite_afficher_raccourcis($dir_module);
			$texte .= "\n</li>" ;
		}
		$texte .= "\n</ul>" ;
	}

	$texte .= "\n</div>" ;
	return $texte ;
}

/**
 * Fonction retournant les blocs de FAQ utilisables pour un plugin donné
 *
 */
function manuelsite_afficher_raccourcis($module = "faq-manuelsite") {
	global $spip_lang;
	
	include_spip('inc/traduire');
	$texte = "\n<ul class=\"faq_blocs\">";
	charger_langue($spip_lang, $module);

	$tableau = $GLOBALS['i18n_' . $module . '_' . $spip_lang];
	ksort($tableau);

	$i = 0;
	foreach ($tableau as $raccourci => $val) {
		if(!preg_match('/_q$/',$raccourci)) {
			$texte .= "\n<li title=\"".texte_backend($val)."\">&lt;faq";
			if($module != "faq-manuelsite") {
				$texte .= "|p=".substr_replace($module,"",0,4);
			}
			$texte .= "|b=$raccourci&gt;</li>";
		}
	}
	$texte .= "\n</ul>" ;

	return $texte;
}
